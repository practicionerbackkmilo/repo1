FROM openjdk:11-jdk
COPY target/apiCurso-0.0.1-SNAPSHOT.jar appV2.jar
EXPOSE 8084
ENTRYPOINT ["java", "-jar", "/appV2.jar"]

