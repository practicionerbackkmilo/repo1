package net.techu;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
public class ProductosController {

    private ArrayList<String> listaProductos = null;

    public ProductosController(){
        listaProductos = new ArrayList<>();
        listaProductos.add("PR1");
        listaProductos.add("PR2");
    }

    /*
    Get Lista de Productos
     */
    //@RequestMapping(value = "/productos", method = RequestMethod.GET, produces = "application/json")
    @GetMapping(value = "/productos", produces = "application/json")
    public ResponseEntity<ArrayList<String>> obtenerListado(){

        System.out.println("Estoy en lista de Productos");;
        return new ResponseEntity(listaProductos, HttpStatus.OK);
    }

    /*
    Get de producto especifico
     *//*
    @RequestMapping(value = "/productos/{id}", method = RequestMethod.GET, produces = "application/json")
    public String obtenerProductoPorId(@PathVariable("id") int id){
        System.out.println("Estoy en obtener Producto especifico");
        String resultado = null;
        try{
            resultado = listaProductos.get(id);
        }
        catch (Exception ex){
            resultado="No se ha encontrado el producto";
        }
        return resultado;


    }*/

    @GetMapping(value = "/productos/{id}", produces = "application/json")
    public ResponseEntity obtenerProductoPorId(@PathVariable("id") int id){
        System.out.println("Estoy en obtener Producto especifico");
        String resultado;
        ResponseEntity respuesta = null;
        try{
            resultado= listaProductos.get(id);
            respuesta = new ResponseEntity<>(resultado, HttpStatus.OK);

        }
        catch (Exception ex){
            resultado="No se ha encontrado el producto";
            respuesta = new ResponseEntity<>(resultado, HttpStatus.NOT_FOUND);
        }

        return respuesta;



    }

    /*
    Add nuevo Producto
    */
    @PostMapping(value = "/productos", produces = "application/json")
    public ResponseEntity AddProducto(){
        System.out.println("Estoy en Añadir");
        listaProductos.add("nuevo");
        return new ResponseEntity<>("Producto creado correctamente",HttpStatus.CREATED);
    }

    /*
    Add nuevo Producto desde URL
    */
    @PostMapping(value = "/productos/{nombre}/{cat}", produces = "application/json")
    public ResponseEntity AddProductoConNombre(@PathVariable("nombre") String nombre, @PathVariable ("cat") String categoria){
        System.out.println("Voy a añadir el produto con nombre:" + nombre + " y categoria:"+categoria );
        listaProductos.add(nombre);
        return new ResponseEntity<>("Producto creado correctamente", HttpStatus.CREATED);
    }

    /*
    PUT Producto especifico por Id
    */
    @PutMapping(value = "/productos/{id}", produces = "application/json")
    public ResponseEntity<String> updateProducto(@PathVariable("id") int id){
        System.out.println("Esoty en actualizar un Producto");
        ResponseEntity<String> resultado = null;

        try{
            String productoAModificar = listaProductos.get(id);
            resultado = new ResponseEntity<>("Producto actulizado correctamente", HttpStatus.OK);
        }
        catch (Exception ex){
            resultado = new ResponseEntity<>("No se ha encontrado el producto", HttpStatus.NOT_FOUND);
        }
        return resultado;
    }

    /*
    DELETE Producto especifico por Id
    */
    @DeleteMapping("/productos/{id}")
    public ResponseEntity<String> deleteProducto(@PathVariable int id)
    {
        ResponseEntity<String> resultado = null;
        try
        {
            String productoAEliminar = listaProductos.get(id);
            resultado = new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        catch (Exception ex)
        {
            resultado = new ResponseEntity<>("No se ha encontrado el producto", HttpStatus.NOT_FOUND);
        }
        return resultado;
    }

}
