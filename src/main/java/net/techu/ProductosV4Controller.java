package net.techu;

import net.techu.data.ProductoMongo;
import net.techu.data.ProductoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.DelegatingServerHttpResponse;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class ProductosV4Controller {

    @Autowired
    private ProductoRepository productoRepository;

    /* Get lista de productos */
    @GetMapping(value = "/v4/productos", produces = "application/json")
    public ResponseEntity<List<ProductoMongo>> obtenerListado()
    {
        List<ProductoMongo> lista = productoRepository.findAll();
        return new ResponseEntity<>(lista, HttpStatus.OK);
    }

    /* Get Encuentra producto por Nombre */
    @GetMapping(value = "/v4/productos/{nombre}", produces = "application/json")
    public ResponseEntity<List<ProductoMongo>> obtenerProductoPorNombre(@PathVariable String nombre)
    {
        if ((productoRepository.findByNombre(nombre)) != null )
            return new ResponseEntity<>(productoRepository.findByNombre(nombre), HttpStatus.OK);
        else
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);

    }

    /* GET Productos por rango de precio */
    @GetMapping(value="/v4/productosbyprecio/{minimo}/{maximo}")
    public ResponseEntity<List<ProductoMongo>> obtenerProductosPorPrecio(@PathVariable double minimo, @PathVariable double maximo)
    {
        List<ProductoMongo> resultado = productoRepository.findByPrecio(minimo, maximo);
        return new ResponseEntity<List<ProductoMongo>>(resultado, HttpStatus.OK);
    }

    /* Add nuevo producto */
    @PostMapping(value = "/v4/productos", produces="application/json")
    public ResponseEntity<String> addProducto(@RequestBody ProductoMongo productoNuevo) {

        try{
            productoRepository.insert(productoNuevo);
            return new ResponseEntity<>("Producto creado correctamente", HttpStatus.CREATED);
        }
        catch (Exception ex){
            List<ProductoMongo> lista = productoRepository.findAll();
            for (ProductoMongo p:lista) {
                if(p.getId().equals(productoNuevo.getId())){
                    return new ResponseEntity<>("El id de producto ya existe", HttpStatus.NOT_ACCEPTABLE);
                }
            }
            return new ResponseEntity<>(ex.toString(),HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    /* PUT actualziacion de producto */
    @PutMapping("/v4/productos/{id}")
    public ResponseEntity<String> updateProducto(@PathVariable("id") String id, @RequestBody ProductoMongo productoModificado)
    {
        //ResponseEntity<String> resultado = null;

        Optional<ProductoMongo> resultado = productoRepository.findById(id);
        if (resultado.isPresent()){
            ProductoMongo productoAModificar = resultado.get();
            productoAModificar.setNombre(productoModificado.getNombre());
            productoAModificar.setPrecio(productoModificado.getPrecio());
            productoRepository.save(productoAModificar);
            return new ResponseEntity<>(productoRepository.save(productoAModificar).toString(), HttpStatus.OK);
        }
        return new ResponseEntity<>("Producto no encontrado", HttpStatus.NOT_FOUND);


    }

    @DeleteMapping("/v4/productos/{id}")
    public ResponseEntity<String> deleteTodos(@PathVariable String id)
    {
        if(productoRepository.existsById(id)){
            productoRepository.deleteById(id);
            return new ResponseEntity<>("Producto borrado", HttpStatus.OK);
        }
        else{
            return new ResponseEntity<>("Producto no encontrado", HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/v4/productos")
    public ResponseEntity<String> deleteTodos()
    {
        productoRepository.deleteAll();
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);

    }
}
