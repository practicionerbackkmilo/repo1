package net.techu.data;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document("productoAndres")
public class ProductoMongo {
    //@Id
    private String id;
    private String nombre;
    private Double precio;

    public ProductoMongo(){
    }

    public ProductoMongo(String nombre, Double precio) {
        this.nombre = nombre;
        this.precio = precio;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Double getPrecio() {
        return precio;
    }

    public void setPrecio(Double precio) {
        this.precio = precio;
    }

    @Override
    public String toString(){
        return String.format("Producto [id=%s, nombre=%s, precio=%s]", id, nombre, precio);
    }
}
